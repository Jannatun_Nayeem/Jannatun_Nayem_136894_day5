<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 11:00
 */

$VarOne = "Welcome to Jarin's world. It's a beautiful day.";
echo"<pre>";
print_r (explode(" ",$VarOne));
echo"</pre>";


$VarTwo = 'one,two,three,four';

// zero limit
echo"<pre>";
print_r(explode(',',$VarTwo,0));
echo"</pre>";

// positive limit
echo"<pre>";
print_r(explode(',',$VarTwo,2));
echo"</pre>";

// negative limit
echo"<pre>";
print_r(explode(',',$VarTwo,-1));
echo"</pre>";
?>