<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 11:01
 */
$numberOne = 9;
$str = "Beijing";
printf("There are %u million bicycles in %s.",$numberOne,$str."<br>"."<br>");

$numberTwo = 123;
printf("%f",$numberTwo."<br>"."<br>"."<br>");
?>

<!--<p>
    Syntax<br><br>
    <i>printf(format,arg1,arg2,arg++)</i><br><br>


    format	Required. Specifies the string and how to format the variables in it.<br>
    Possible format values:<br>

    %% - Returns a percent sign<br>
    %b - Binary number
    %c - The character according to the ASCII value<br>
    %d - Signed decimal number (negative, zero or positive)<br>
    %e - Scientific notation using a lowercase (e.g. 1.2e+2)<br>
    %E - Scientific notation using a uppercase (e.g. 1.2E+2)<br>
    %u - Unsigned decimal number (equal to or greather than zero)<br>
    %f - Floating-point number (local settings aware)<br>
    %F - Floating-point number (not local settings aware)<br>
    %g - shorter of %e and %f<br>
    %G - shorter of %E and %f<br>
    %o - Octal number<br>
    %s - String<br>
    %x - Hexadecimal number (lowercase letters)<br>
    %X - Hexadecimal number (uppercase letters)<br>
    Additional format values. These are placed between the % and the letter (example %.2f):<br>

    + (Forces both + and - in front of numbers. By default, only negative numbers are marked)<br>
    ' (Specifies what to use as padding. Default is space. Must be used together with the width specifier. Example: %'x20s (this uses "x" as padding)<br>
    - (Left-justifies the variable value)<br>
    [0-9] (Specifies the minimum width held of to the variable value)<br>
    .[0-9] (Specifies the number of decimal digits or maximum string length)<br>
    Note: If multiple additional format values are used, they must be in the same order as above.<br>
</p>-->
