<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 09:17
 */

$a=3;
$b=7;
// One way to diclare
if($a != $b){
    echo "The value a = $a and b =$b are not equal. ".'Result is TRUE : if $a is not equal to $b after type juggling'."<br>"."<br>";
}
else{
    echo "The value a = $a and b =$b are equal."."<br>"."<br>";
}

// Onther way to diclare
if($a <> $b){
    echo "The value a = $a and b =$b are not equal. ".'Result is TRUE : if $a is not equal to $b after type juggling'."<br>"."<br>";
}
else{
    echo "The value a = $a and b =$b are equal.";
}
?>