<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 10:33
 */
$a = 5;
$b = 3;

if($a = 3 xor $b = 2){
    echo 'Result is TRUE : if either $a or $b is TRUE, but not both.'."<br>"."<br>";
}
else{
    echo 'Result is FALSE : if either $a or $b or both are FALSE.'."<br>"."<br>";
}

?>