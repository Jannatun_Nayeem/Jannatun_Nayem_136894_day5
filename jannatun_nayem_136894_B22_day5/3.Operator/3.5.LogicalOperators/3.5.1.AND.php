<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 10:32
 */

$a = 5;
$b = 3;

if($a = 5 and $b = 3){
    echo 'Result is TRUE : if both $a and $b are TRUE.'."<br>"."<br>";
}
else{
    echo 'Result is FALSE : if both $a and $b are FALSE.'."<br>"."<br>";
}

if($a = 5 && $b = 3){
    echo 'Result is TRUE : if both $a and $b are TRUE.'."<br>"."<br>";
}
else{
    echo 'Result is FALSE : if both $a and $b are FALSE.';
}
?>