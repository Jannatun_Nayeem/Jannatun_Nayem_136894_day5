<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 10:32
 */

$a = 5;
$b = 3;

if($a = 3 or $b = 4){
    echo 'Result is TRUE : if either $a or $b is TRUE.'."<br>"."<br>";
}
else{
    echo 'Result is FALSE : if either $a or $b is FALSE.'."<br>"."<br>";
}

if($a = 4 || $b = 5){
    echo 'Result is TRUE : if either $a or $b is TRUE.'."<br>"."<br>";
}
else{
    echo 'Result is FALSE : if either $a or $b is FALSE.';
}

?>