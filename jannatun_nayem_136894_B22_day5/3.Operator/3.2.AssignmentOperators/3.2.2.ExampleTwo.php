<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 08:46
 */

$a = 7;
$b = 3;

$a &= $b;  // $a = $a & $b  Bitwise And
echo 'The Bitwise And value of $a &= $b is : ' .$a."<br>"."<br>"; // Out put : 3

$a |= $b;  // $a = $a | $b  Bitwise Or
echo 'The Bitwise Or value of $a |= $b is : ' .$a."<br>"."<br>"; // Out put : 3

$a ^= $b;  // $a = $a ^ $b  Bitwise Xor
echo 'The Bitwise Xor value of $a ^= $b is : ' .$a."<br>"."<br>"; // Out put : 0

$a <<= $b; // $a = $a << $b Left shift
echo 'The Left shift value of $a <<= $b is : ' .$a."<br>"."<br>"; // Out put : 0

$a >>= $b; // $a = $a >> $b Right shift
echo 'The Right shift value of $a >>= $b is : ' .$a."<br>"."<br>"; // Out put : 0

?>