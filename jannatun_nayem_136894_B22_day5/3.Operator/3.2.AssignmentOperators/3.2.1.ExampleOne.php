<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-19
 * Time: 08:46
 */
$a = 7;
$b = 3;

$a += $b; // $a = $a + $b  Addition
echo 'The Addition value of $a += $b is : ' .$a."<br>"."<br>"; // Out put : 10

$a -= $b; // $a = $a - $b  Subtraction
echo 'The Subtraction value of $a -= $b is : ' .$a."<br>"."<br>"; // Out put : 7

$a *= $b; // $a = $a * $b  Multiplication
echo 'The Multiplication value of $a *= $b is : ' .$a."<br>"."<br>"; // Out put : 21

$a /= $b; // $a = $a / $b  Division
echo 'The Division value of $a /= $b is : ' .$a."<br>"."<br>"; // Out put : 7

$a %= $b; // $a = $a % $b  Modulus
echo 'The Modulus value of $a %= $b is : ' .$a."<br>"."<br>"; // Out put : 1

$a .= $b; // $a = $a . $b  Concatenate
echo 'The Concatenate value of $a .= $b is : ' .$a."<br>"."<br>"; // Out put : 13

?>