<?php
/**
 * Created by PhpStorm.
 * User: jarin
 * Date: 2016-05-18
 * Time: 17:16
 */
namespace My1stNameSpace
{
    Class MyClass {
        function MyFunction(){
            echo "My 1st Name Space name is : ".__NAMESPACE__;
            echo "<br>"."<br>";
        }
    }

    $obj = new MyClass();
    $obj->MyFunction();
//echo $obj;
}

namespace My2ndNameSpace
{
    Class MyClass {
        function MyFunction(){
            echo "My 2nd Name Space name is : ".__NAMESPACE__;
        }
    }

    $obj = new MyClass();
    $obj->MyFunction();
//echo $obj;
}

?>